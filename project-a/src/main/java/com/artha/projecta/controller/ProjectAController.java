package com.artha.projecta.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * Project A REST Controller
 * @author : R. C. Bharti
 */
@RestController
@RequestMapping(value = "/project-a")
public class ProjectAController {

    @RequestMapping(value = "/getProjectInfo", method = RequestMethod.GET)
    public String getProjectInfo(){
        return "ProjectA";
    }
}
