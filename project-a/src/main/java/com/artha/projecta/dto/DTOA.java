package com.artha.projecta.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object A
 * @author : R. C. Bharti
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DTOA {
    private String testA;
}
