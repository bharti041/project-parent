package com.artha.projecta.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object B
 * @author : R. C. Bharti
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DTOB {
    private String testB;
}
