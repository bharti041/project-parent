# spring-boot-dockerize
How to Dockerize Spring Boot Application 

# Build Docker Image 
$ docker build -t project-a.jar .

# Check Docker Image 
$ docker image ls

# Run Docker Image 
$ docker run -p 7123:8081 project-a.jar.jar

In the run command, we have specified that the port 8081 on the container should be mapped to the port 7123 on the Host OS.
