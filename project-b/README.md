# spring-boot-dockerize
How to Dockerize Spring Boot Application 

# Build Docker Image 
$ docker build -t project-b.jar .

# Check Docker Image 
$ docker image ls

# Run Docker Image 
$ docker run -p 8123:8082 project-b.jar.jar

In the run command, we have specified that the port 8082 on the container should be mapped to the port 8123 on the Host OS.
