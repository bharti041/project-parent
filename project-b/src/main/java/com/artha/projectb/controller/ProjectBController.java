package com.artha.projectb.controller;

import com.artha.projecta.dto.DTOA;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project B REST Controller
 * @author : R. C. Bharti
 *
 */
@RestController
@RequestMapping(value = "/project-b")
public class ProjectBController {

    @RequestMapping(value = "/getDTOA", method = RequestMethod.GET)
    public DTOA getDTOAInstance(){
        return new DTOA("testA");
    }
}
