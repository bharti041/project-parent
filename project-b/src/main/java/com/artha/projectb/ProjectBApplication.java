package com.artha.projectb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Project B main application
 * @author : R. C. Bharti
 *
 */
@SpringBootApplication
public class ProjectBApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectBApplication.class, args);
	}

}
