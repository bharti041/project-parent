# spring-boot-dockerize
How to Dockerize Spring Boot Application 

# Build Docker Image 
$ docker build -t project-c.jar .

# Check Docker Image 
$ docker image ls

# Run Docker Image 
$ docker run -p 9123:8083 project-c.jar.jar

In the run command, we have specified that the port 8083 on the container should be mapped to the port 9123 on the Host OS.
