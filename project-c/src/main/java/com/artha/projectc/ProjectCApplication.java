package com.artha.projectc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Project C main application
 * @author : R. C. Bharti
 *
 */
@SpringBootApplication
public class ProjectCApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCApplication.class, args);
	}

}
