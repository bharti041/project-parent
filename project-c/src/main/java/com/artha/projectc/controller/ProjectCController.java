package com.artha.projectc.controller;

import com.artha.projecta.dto.DTOB;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project C REST Controller
 * @author : R. C. Bharti
 *
 */
@RestController
@RequestMapping(value = "/project-c")
public class ProjectCController {

    @RequestMapping(value = "/getDTOB", method = RequestMethod.GET)
    public DTOB getDTOAInstance(){
        return new DTOB("testB");
    }
}
